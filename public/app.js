console.log("This is working!");

(function () {

  var myConnector = tableau.makeConnector();

  myConnector.getSchema = function (schemaCallback) {
    const columnas = [
      {
        id: "id",
        dataType: tableau.dataTypeEnum.string,
      },
      {
        id: "name",
        dataType: tableau.dataTypeEnum.string,
      },
      {
        id: "type",
        dataType: tableau.dataTypeEnum.int,
      },
      {
        id: "active",
        dataType: tableau.dataTypeEnum.bool,
      },
    ];

    let covidTableSchema = {
      id: "KN",
      alias: "Checklist",
      columns: columnas,
    };

    schemaCallback([covidTableSchema]);
  };

  myConnector.getData = function (table, doneCallback) {
    let tableData = [];
    var i = 0;
    var res = "";
    $.ajax({
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", 'application/json');
        request.setRequestHeader("Authorization", 'Bearer token');
    },
      url:"https://integration.checklistfacil.com.br/v2/checklists/",
      type: "GET",
      success: function (result) {
        console.log(result);
        //console.log(result["data"].length);
        data = result["data"];
        for (i = 0, len = data.length; i < len; i++) {
          console.log(i);
          tableData.push({
            id: data[i].id,
            name: data[i].name,
            type: data[i].type,
            active: data[i].active,
          });
        }
        table.appendRows(tableData);
            doneCallback();
      },
      error: function(jqXHR,textStatus,errorThrown) {
        console.log(jqXHR.status);
        console.log(textStatus);
        console.log(errorThrown);
        doneCallback();
      }
    });
    
    
  }
  tableau.registerConnector(myConnector);
})();

document.querySelector("#getData").addEventListener("click", getData);

function getData() {
  tableau.connectionName = "KN checklist";
  tableau.submit();
}
